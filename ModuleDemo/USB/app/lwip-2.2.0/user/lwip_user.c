#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lwip/opt.h"
#include "FreeRTOS.h"
#include "task.h"
#include "lwip/def.h"
#include "lwip/mem.h"
#include "lwip/pbuf.h"
#include "lwip/stats.h"
#include "lwip/snmp.h"
#include "lwip/ethip6.h"
#include "lwip/etharp.h"
#include "lwip/tcpip.h"
#include "netif/ppp/pppoe.h"

struct netif gnetif;
ip4_addr_t ipaddr;
ip4_addr_t netmask;
ip4_addr_t gw;

struct netif *g_glb_netif;

u32_t lwip_fuzz_rand(void)
{
    return 0;
}


err_t
ethernetif_init(struct netif *netif)
{
  return ERR_OK;
}

void TCPIP_Init(void)
{  
  /* IP addresses initialization */
  /* USER CODE BEGIN 0 */
#if 0 //LWIP_DHCP
  ip_addr_set_zero_ip4(&ipaddr);
  ip_addr_set_zero_ip4(&netmask);
  ip_addr_set_zero_ip4(&gw);
#else
  IP4_ADDR(&ipaddr, 192, 168, 3, 1);
  IP4_ADDR(&netmask, 255, 255, 255, 0);
  IP4_ADDR(&gw, 192, 168, 3, 1);
#endif /* USE_DHCP */
  tcpip_init(NULL, NULL);
  /* USER CODE END 0 */
  /* Initilialize the LwIP stack without RTOS */
  /* add the network interface (IPv4/IPv6) without RTOS */
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);

  /* Registers the default network interface */
  netif_set_default(&gnetif);

  if (netif_is_link_up(&gnetif))
  {
    /* When the netif is fully configured this function must be called */
    printf("netif_set_up\r\n");
    netif_set_up(&gnetif);
  }
  else
  {
    /* When the netif link is down this function must be called */
    printf("netif_set_down\r\n");
    netif_set_down(&gnetif);
  }
  
#if 0// LWIP_DHCP	   			// use dhcp 
  int err;
  /*  Creates a new DHCP client for this interface on the first call.
  Note: you must call dhcp_fine_tmr() and dhcp_coarse_tmr() at
  the predefined regular intervals after starting the client.
  You can peek in the netif->dhcp struct for the actual DHCP status.*/
  
  printf("dynamic addr, set LWIP_DHCP to 0 in lwipopts.h\n\n");
  
  err = dhcp_start(&gnetif);      //����dhcp
  if(err == ERR_OK)
    printf("lwip dhcp init success...\n\n");
  else
    printf("lwip dhcp init fail...\n\n");
  while(ip_addr_cmp(&(gnetif.ip_addr),&ipaddr))   // wait ip avaible
  {
    osDelay(1);
  } 
#endif
  printf("local ip:%d.%d.%d.%d\n\n",  \
        ((gnetif.ip_addr.addr)&0x000000ff),       \
        (((gnetif.ip_addr.addr)&0x0000ff00)>>8),  \
        (((gnetif.ip_addr.addr)&0x00ff0000)>>16), \
        ((gnetif.ip_addr.addr)&0xff000000)>>24);
}
