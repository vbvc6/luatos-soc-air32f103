#include "env_const.h"

#ifndef _FIX_VOLTAGE_H_
#define _FIX_VOLTAGE_H_

typedef enum {
    VOL_0V,
    VOL_1V,
    VOL_3P3V,
    VOL_5V,
    VOL_6V,
    VOL_9V,
    VOL_12V,
    VOL_15V,
    VOL_18V,
    VOL_20V,
    VOL_24V,
    VOL_25V,
    VOL_BUTT,
} fix_voltage;

float fix_voltage_get_value(int index);
const char *fix_voltage_get_name(int index);
int fix_voltage_get_dac(int index);

#endif

