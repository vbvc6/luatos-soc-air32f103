#include "key.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu.h"
#include "ui_menu_cal.h"
#include "ui_menu_backlight.h"
#include "ui_menu_voltage.h"
#include "ui_menu_current.h"
#include "ui_menu_update.h"
#include "ui_menu_wave.h"
#include "ui_menu_iv.h"
#include "ui_menu_debug.h"

uint32_t g_ui_main_state;

const menu_item g_menus[] = {
    {"Update firmware", menu_update_firmware},
    {"Voltage sel", menu_voltage_select},
    {"Current set", menu_current_set},
    {"Voltage set", menu_voltage_set},
    {"Vol Cali", menu_vol_calibration},
    {"Cur Cali", menu_cur_calibration},
    {"All wave", menu_wave_show_all},
    {"Voltage wave", menu_wave_show_voltage},
    {"Current wave", menu_wave_show_current},    
    {"Current debug", menu_current_debug},
    {"IV Measure", menu_iv_test},
    //{"Back Light", menu_backlight_test},
};

void ui_menu_poll(void)
{    
    ui_menu_entry("===== Main Menu =====", g_menus, sizeof(g_menus) / sizeof(g_menus[0]));
}
