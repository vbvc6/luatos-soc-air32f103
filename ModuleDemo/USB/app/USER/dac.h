#include "air32f10x.h"

#ifndef _DAC_H_
#define _DAC_H_

enum dac_current_level {
    DAC_CURRENT_LEVEL_L,
    DAC_CURRENT_LEVEL_H,
    DAC_CURRENT_LEVEL_BUTT
};

void dac_init(void);
void dac_set_value(int voltage, int current);
void dac_voltage_set_value(float voltage);
void dac_current_set_value(float current);
void dac_set_voltage(float voltage);
void dac_set_current(float current);
float dac_voltage_get_value(void);
float dac_current_get_value(void);
void dac_voltage_set_raw_value(int voltage);
void dac_current_set_raw_value(int current);
int dac_voltage_get_raw_value(void);
int dac_current_get_raw_value(void);
float dac_current_get_max(void);

#endif

