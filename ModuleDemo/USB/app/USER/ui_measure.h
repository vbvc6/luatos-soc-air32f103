#include "air32f10x.h"

#ifndef _UI_MEASURE_H_
#define _UI_MEASURE_H_

uint32_t ui_measure_get_mini(uint16_t *data);
uint32_t ui_measure_get_max(uint16_t *data);
uint32_t ui_measure_get_cycle(uint16_t *data);

#endif

