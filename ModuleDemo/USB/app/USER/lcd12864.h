#include "system.h"

#ifndef _LCD12864_H_
#define _LCD12864_H_

void lcd12864_init(void);
void lcd12864_set_point_value(int x, int y, int value);
void lcd12864_set_point(int x, int y);
void lcd12864_clear_point(int x, int y);
void lcd12864_clear(void);
void lcd12864_back_light(int en);
void lcd12864_back_light_toggle(void);

#endif

