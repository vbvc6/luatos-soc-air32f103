#include <stdint.h>
#include "singlelist.h"

#ifndef _ICON_H_
#define _ICON_H_

void icon_show(int x, int y, int width, int height, const char *icon);
uint32_t icon_show_bit_string(int x, int y, char *string);

#endif

