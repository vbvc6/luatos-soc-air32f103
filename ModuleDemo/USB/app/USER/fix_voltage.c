#include <stdlib.h>
#include <stdio.h>
#include "fix_voltage.h"
#include "env.h"

float fix_voltage_get_value(int index)
{
    const float voltage[VOL_BUTT] = {
        0.0,
        1.0,
        3.3,
        5.0,
        6.0,
        9.0,
        12.0,
        15.0,
        18.0,
        20.0,
        24.0,
        25.0,
    };
    if (index < 0) {
        return voltage[0];
    }
    if (index >= VOL_BUTT) {
        return voltage[VOL_BUTT - 1];
    }
    return voltage[index];
}

const char *fix_voltage_get_name(int index)
{
    const char *names[] = {
        "0V", 
        "1V", 
        "3.3V",
        "5V", 
        "6V", 
        "9V",
        "12V", 
        "15V",
        "18V", 
        "20V", 
        "24V", 
        "25V", 
    };
    if (index < 0) {
        return names[0];
    }
    if (index >= VOL_BUTT) {
        return names[VOL_BUTT - 1];
    }
    return names[index];
}

int fix_voltage_get_dac(int index)
{
    const int defaults[VOL_BUTT] = {
        3623, // 0.0
        3487, // 1.0
        3154, // 3.3
        2909, // 5.0
        2764, // 6.0
        2330, // 9.0 --
        1500, // 12.0
        1000, // 15.0
        800,  // 18.0
        500,  // 20.0
        100,  // 24.0
        0,    // 25.0
    };
    char buffer[12];
    int value;
    
    if (env_get((char *)fix_voltage_get_name(index), buffer, sizeof(buffer))) {
        value = atoi(buffer);
        return value;
    }
    sprintf(buffer, "%d", defaults[index]);
    env_set((char *)fix_voltage_get_name(index), buffer);
    return defaults[index];
}
