#include <stdlib.h>
#include <stdio.h>
#include "air32f10x_gpio.h"
#include "time.h"
#include "dac.h"
#include "env.h"
#include "fix_voltage.h"

#define VOLTAGE_SCALE_H     2.0
#define VOLTAGE_SCALE_L     12.886

#define CURRENT_SCALE_L     (4096.0 / 0.05)
#define CURRENT_SCALE_H     (4096.0 / 2.0)

static float g_dac_voltage;
static float g_dac_current;

static uint16_t g_dac_voltage_raw;
static uint16_t g_dac_current_raw;

//static int g_dac_voltage_level;
static float g_dac_current_max;

void dac_init(void)
{
    GPIO_InitTypeDef gpio;
    DAC_InitTypeDef dac;

    gpio.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio);

    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;

    // current
    gpio.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14;
    GPIO_Init(GPIOC, &gpio);
    GPIO_SetBits(GPIOC, GPIO_Pin_13); 
    GPIO_ResetBits(GPIOC, GPIO_Pin_14); 
    //GPIO_SetBits(GPIOC, GPIO_Pin_13); 
    //GPIO_SetBits(GPIOC, GPIO_Pin_14); 
    
    // voltage
    gpio.GPIO_Pin = GPIO_Pin_11;
    GPIO_Init(GPIOB, &gpio);
    GPIO_ResetBits(GPIOB, GPIO_Pin_11); 
    //GPIO_SetBits(GPIOB, GPIO_Pin_11); 

    dac.DAC_Trigger = DAC_Trigger_None;
    dac.DAC_WaveGeneration = DAC_WaveGeneration_None;
    dac.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
    dac.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
    DAC_Init(DAC_Channel_1, &dac);
    DAC_Init(DAC_Channel_2, &dac);
    DAC_SetDualChannelData(DAC_Align_12b_R, 25000, 0); 
    DAC_Cmd(DAC_Channel_1, ENABLE);
    DAC_Cmd(DAC_Channel_2, ENABLE);
}

static uint16_t dac_voltage_convert(float voltage)
{
    char buffer[32];
    int start = 0, end = 0;    
    
    if (voltage > fix_voltage_get_value(VOL_BUTT - 1)) {
        voltage = fix_voltage_get_value(VOL_BUTT - 1);
    }
    if (voltage < fix_voltage_get_value(0)) {
        voltage = fix_voltage_get_value(0);
    }
    for (int i = 0; i < (VOL_BUTT - 1); i++) {
        if (voltage >= fix_voltage_get_value(i)) {
            start = i;
        }
    }
    sprintf(buffer, "%d", (int)(voltage * 1000));
    env_set(VOL_DEF, buffer);
    end = (fix_voltage)(start + 1);
    float scale = (voltage - fix_voltage_get_value(start)) / 
        (fix_voltage_get_value(end) - fix_voltage_get_value(start));
    int dac = fix_voltage_get_dac(start) + 
        (fix_voltage_get_dac(end) - fix_voltage_get_dac(start)) * scale;    

    return dac;
}

void dac_voltage_set_value(float voltage)
{
    g_dac_voltage = voltage;
    g_dac_voltage_raw = dac_voltage_convert(voltage);
    DAC_SetChannel1Data(DAC_Align_12b_R, dac_voltage_convert(voltage)); 
}

void dac_voltage_set_raw_value(int voltage)
{
    DAC_SetChannel1Data(DAC_Align_12b_R, voltage); 
}

float dac_voltage_get_value(void)
{
    return g_dac_voltage;
}

int dac_voltage_get_raw_value(void)
{
    return g_dac_voltage_raw;
}

float dac_current_get_max(void)
{
    return g_dac_current_max;
}

static uint16_t dac_current_convert(float current)
{
    char buffer[32];

    if (current < 0.05) {
        GPIO_ResetBits(GPIOC, GPIO_Pin_13); 
        GPIO_SetBits(GPIOC, GPIO_Pin_14); 
        g_dac_current_max = 0.05;
    } else {
        GPIO_SetBits(GPIOC, GPIO_Pin_13); 
        GPIO_ResetBits(GPIOC, GPIO_Pin_14); 
        g_dac_current_max = 2.0;
    }
    sprintf(buffer, "%d", (int)(current * 1000));
    env_set(CUR_DEF, buffer);
    
    return current * 4095 / g_dac_current_max;
}

void dac_current_set_value(float current)
{
    g_dac_current = current;
    g_dac_current_raw = dac_current_convert(current);
    DAC_SetChannel2Data(DAC_Align_12b_R, g_dac_current_raw); 
}

void dac_current_set_raw_value(int current)
{
    DAC_SetChannel2Data(DAC_Align_12b_R, current); 
}

float dac_current_get_value(void)
{
    return g_dac_current;
}

int dac_current_get_raw_value(void)
{
    return g_dac_current_raw;
}
