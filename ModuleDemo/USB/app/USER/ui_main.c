#include <stdlib.h>
#include <stdio.h>
#include "adc.h"
#include "time.h"
#include "signal.h"
#include "wave.h"
#include "key.h"
#include "ui_main.h"
#include "icon.h"
#include "display.h"
#include "ui_measure.h"
#include "ui_menu_main.h"
#include "adc.h"
#include "regulator.h"

void ui_main_init(void)
{
}

void ui_main_key_proc(void)
{
    int key;
    
    key = key_wait(1000);
    if (key == KEY_RIGHT) {
        lcd12864_back_light(0);
    }
    if (key == KEY_LEFT) {
        lcd12864_back_light(1);
    }
    if (key == KEY_MENU) {
        ui_menu_poll();
    }
    if (key == KEY_OK) {
        regulator_toggle();
    }
}

void ui_main_format(char *buffer, float value, char unit)
{
    float data;

    if (value < 0.001) {
        data = value * 1000 * 1000;
        sprintf(buffer, "%f", data);
        buffer[5] = 'u';
        buffer[6] = unit;
        buffer[7] = 0;
    } else if (value < 1.0) {
        data = value * 1000;
        sprintf(buffer, "%f", data);
        buffer[5] = 'm';
        buffer[6] = unit;
        buffer[7] = 0;
    } else {
        data = value;
        sprintf(buffer, "%f", data);
        buffer[5] = unit;
        buffer[6] = 0;
    }    
}

void ui_scope_show_main(void)
{
    char buffer[30];
    float data;

    data = regulator_get_current();
    ui_main_format(buffer, data, 'A');
    icon_show_bit_string(16, 0, buffer);  

    data = regulator_get_voltage();
    ui_main_format(buffer, data, 'V');
    icon_show_bit_string(16, 32, buffer);  
}


void ui_main_poll(void)
{
    lcd12864_clear();
    ui_scope_show_main();           
    ui_main_key_proc(); 
}
