#include "air32f10x.h"

#ifndef _UI_INPUT_H_
#define _UI_INPUT_H_

typedef void (*floatCb)(int);
typedef void (*intCb)(int);

float ui_input_float(const char *title, float current, float min, float max, float step, floatCb cb);
int ui_input_int(const char *title, int current, int min, int max, int step, intCb cb);
int ui_input_edit_int(const char *title, int current, int max, intCb cb);

#endif

