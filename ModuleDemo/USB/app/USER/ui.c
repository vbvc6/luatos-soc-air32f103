#include "lcd12864.h"
#include "time.h"
#include "ui_main.h"
#include "ui_menu_main.h"
   
void ui_poll(void)
{   
    while (1) {
        ui_main_poll();
    }
}

void ui_init(void)
{
    ui_main_init();
}
