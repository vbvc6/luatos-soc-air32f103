#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu_backlight.h"

int menu_backlight_test(void *data)
{
    int key;
    
    while (1) {
        lcd12864_clear();
        display_string(0, 56, "backlight....");
        key = key_wait(0);
        if (key  == KEY_OK) {
            return 0;
        }    
    }
}
