#include "system.h"
#include "air32f10x_adc.h"
#include "air32f10x_gpio.h"

#ifndef _ADC_H_
#define _ADC_H_

void adc_init(void);
void adc_set_period(uint32_t ns);
uint32_t adc_get_period_ns(void);
void adc_start(void);
void adc_stop(void);
float adc_get_voltage(float target);
float adc_get_current(float limit);
void adc_get_buffer(uint16_t *buffer, uint32_t from, uint32_t cnt);
void adc_get_realtime_voltage_wave(uint16_t *buffer);
void adc_get_realtime_current_wave(uint16_t *buffer);
void adc_get_realtime_all_wave(uint16_t *voltage, uint16_t *current);

#endif

