#include "air32f10x.h"

#ifndef _SINGLE_LIST_H_
#define _SINGLE_LIST_H_

typedef struct _list {
    struct _list *next;
} list;

void list_add(list **head, list *item);
void list_remove(list **head, list *item);

#endif

