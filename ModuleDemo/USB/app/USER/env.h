#include "air32f10x.h"
#include "env_const.h"

#ifndef _ENV_H_
#define _ENV_H_

char *env_get(char *name, char *buffer, int len);
char *env_get_bin(char *bname, int lname, char *bvalue, int lvalue);
char *env_get_bin_value(char *name, char *bvalue, int lvalue);
void env_set(char *name, char *value);
void env_set_bin(char *bname, int lname, char *bvalue, int lvalue);
void env_set_bin_value(char *name, char *bvalue, int lvalue);
void env_clear(void);

#endif

