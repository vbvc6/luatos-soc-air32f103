#include "singlelist.h"

void list_add(list **head, list *item)
{
    item->next = *head;
    *head = item;
}

void list_remove(list **head, list *item)
{
    list *it = *head;
    
    if (item == *head) {
        *head = item->next;
        return;
    }
    while (it) {
        if (it->next == item) {
            it->next = item->next;
            return;
        }
        it = it->next;
    }
}

