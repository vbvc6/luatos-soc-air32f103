#include "time.h"
#include "env.h"
#include "air32f10x_flash.h"
#include <string.h>

// last 2k
#define ENV_START_ADDR 0x0803F000
#define ENV_SIZE 4096

static uint32_t g_env_buffer[ENV_SIZE / 4];

static char *env_data_get(char *buffer, int len)
{
    memcpy(buffer, (char *)ENV_START_ADDR, len);
    return buffer;
}

static void env_data_set(char *buffer, int len)
{
    int cnt;
    uint32_t *pdata;
    
    pdata = (uint32_t *)buffer;
    FLASH_Unlock();
    FLASH_ErasePage(ENV_START_ADDR);
    for(cnt = 0; cnt < ((len + 3) / 4); cnt++) {
        FLASH_ProgramWord(ENV_START_ADDR + cnt * 4, pdata[cnt]);
    }    
}

static int env_len(char *env)
{
    int cnt = 0;

    while((0 != *env) && (0xff != (unsigned char)*env)) {
        cnt++;
        env++;
    }
    return cnt;
}

char *env_get(char *name, char *buffer, int blen)
{
    char *env;
    int len;
    char *mbuf;

    mbuf = (char *)g_env_buffer;
    env = mbuf;
    env_data_get(env, ENV_SIZE);
    len = env_len(name);
    while(1) {
        if(!env[0] || (0xff == (unsigned char)env[0])
           || env >= &mbuf[ENV_SIZE]) {
            return 0;
        }
        if(!strncmp(env, name, len) && '=' == env[len]) {
            strncpy(buffer, env + len + 1, blen);
            return buffer;
        }
        env = env + env_len(env) + 1;
    }
}

static char env_hex_to_bin_chr(char hex)
{
    if(hex >= '0' && hex <= '9')
        return hex - '0';
    if(hex >= 'a' && hex <= 'f')
        return hex - 'a' + 10;
    if(hex >= 'A' && hex <= 'F')
        return hex - 'A' + 10;
    return 0;
}

static char env_bin_to_hex_chr(char bin)
{
    if((signed char)bin >= 0 && bin <= 9) {
        return bin + '0';
    }
    if(bin >= 10 && bin <= 15) {
        return bin - 10 + 'a';
    }
    return '0';
}

static void env_hex_to_bin(char *hex, char *bin, int blen)
{
    int i;

    for(i = 0; i < blen; i++) {
        if(!hex[2 * i] || !hex[2 * i + 1]) {
            return;
        }
        bin[i] = env_hex_to_bin_chr(hex[2 * i + 1])
                 + (env_hex_to_bin_chr(hex[2 * i]) << 4);
    }
}


static void env_bin_to_hex(char *bin, char *hex, int blen)
{
    int i;

    for(i = 0; i < blen; i++) {
        hex[i * 2 + 1] = env_bin_to_hex_chr(bin[i] & 0xf);
        hex[i * 2] = env_bin_to_hex_chr((bin[i] >> 4) & 0xf);
    }
    hex[2 * blen] = 0;
}


void env_set(char *name, char *value)
{
    char *buffer, *oldbuffer, *env, *p;
    int len;

    buffer = (char *)g_env_buffer;
    oldbuffer = (char *)ENV_START_ADDR;
    env = oldbuffer;
    memset(buffer, 0, ENV_SIZE);
    p = buffer;
    len = env_len(name);

    if(value && value[0]) {
        strcpy(p, name);
        p = p + env_len(p);
        p[0] = '=';
        p++;
        strcpy(p, value);
        p = p + env_len(p) + 1;
    }

    while(1) {
        if(!env[0] || 0xff == (unsigned char)env[0]
           || p >= &buffer[ENV_SIZE]
           || env >= &oldbuffer[ENV_SIZE])
            break;
        if(!strncmp(env, name, len) && '=' == env[len]) {
            env = env + env_len(env) + 1;
            continue;
        }
        strcpy(p, env);
        env = env + env_len(env) + 1;
        p = p + env_len(p) + 1;
    }
    env_data_set(buffer, p - buffer + 2);
}

char *env_get_bin(char *bname, int lname, char *bvalue, int lvalue)
{
    char name[2 * lname + 1], value[2 * lvalue + 1];

    env_bin_to_hex(bname, name, lname);
    memset(bvalue, 0, lvalue);
    if(env_get(name, value, 2 * lvalue + 1)) {
        env_hex_to_bin(value, bvalue, lvalue);
        return bvalue;
    }
    return 0;
}


char *env_get_bin_value(char *name, char *bvalue, int lvalue)
{
    char value[2 * lvalue + 1];

    memset(bvalue, 0, lvalue);
    if(env_get(name, value, 2 * lvalue + 1)) {
        env_hex_to_bin(value, bvalue, lvalue);

        return bvalue;
    }
    return 0;
}


void env_set_bin(char *bname, int lname, char *bvalue, int lvalue)
{
    char name[2 * lname + 1], value[2 * lvalue + 1];

    env_bin_to_hex(bname, name, lname);
    env_bin_to_hex(bvalue, value, lvalue);
    env_set(name, value);
}


void env_set_bin_value(char *name, char *bvalue, int lvalue)
{
    char value[2 * lvalue + 1];

    env_bin_to_hex(bvalue, value, lvalue);
    env_set(name, value);
}

void env_clear(void)
{
    char buffer[4];

    memset(buffer, 0, sizeof(buffer));
    env_data_set(buffer, sizeof(buffer));
}
