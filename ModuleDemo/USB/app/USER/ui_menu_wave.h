#include "air32f10x.h"

#ifndef _UI_MENU_WAVE_H_
#define _UI_MENU_WAVE_H_

int menu_wave_show_all(void *data);
int menu_wave_show_voltage(void *data);
int menu_wave_show_current(void *data);

#endif

