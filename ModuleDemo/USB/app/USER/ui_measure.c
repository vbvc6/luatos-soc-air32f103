#include "ui_measure.h"

uint32_t ui_measure_get_mini(uint16_t *data)
{
    uint32_t min = data[0];
    
    for (int i = 0; i < 128; i++) {
        if (min > data[i]) {
            min = data[i];
        }
    }
    return min;
}

uint32_t ui_measure_get_max(uint16_t *data)
{
    uint32_t min = data[0];
    
    for (int i = 0; i < 128; i++) {
        if (min < data[i]) {
            min = data[i];
        }
    }
    return min;
}

uint32_t ui_measure_get_cycle(uint16_t *data)
{
    uint32_t average = 0;
    uint32_t last, cur, start = 0, end = 0;
    
    for (int i =  0; i < 128; i++) {
        average += data[i];
    }
    average = average /128;
    last = data[0] > average;
    for (int i = 0; i < 128; i++) {
        cur = data[i] > average;
        if (cur != last) {
            start = i;
            end = i;
            last = cur;
            break;
        }
    }
    
    for (int i = start; i < 128; i++) {
        cur = data[i] > average;
        if (cur != last) {
            end = i;
            break;
        }
    }
    if (end == start) {
        return 128;
    }
    return (end - start) * 2;
}
