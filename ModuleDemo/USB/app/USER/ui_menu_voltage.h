#include "air32f10x.h"

#ifndef _UI_MENU_VOLTAGE_H_
#define _UI_MENU_VOLTAGE_H_

int menu_voltage_select(void *data);
int menu_voltage_set(void *data);

#endif

