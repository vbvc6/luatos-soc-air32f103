#include "time.h"
#include "dac.h"
#include "adc.h"
#include "regulator.h"
#include "env.h"
#include <stdlib.h>
#include <stdio.h>
#include "air32f10x_gpio.h"
#include "env_const.h"

void regulator_init_default(void)
{
    char buffer[12];
    float value;

    if (env_get(VOL_DEF, buffer, sizeof(buffer))) {
        value = atoi(buffer) / 1000.0;
    } else {
        value = 3.3;
    }
    regulator_set_voltage(value);

    if (env_get(CUR_DEF, buffer, sizeof(buffer))) {
        value = atoi(buffer) / 1000.0;        
    } else {
        value = 0.01;
    }
    regulator_set_current(value);
}

void regulator_init(void)
{
    GPIO_InitTypeDef gpio;    
    
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    // en
    gpio.GPIO_Pin = GPIO_Pin_9;
    GPIO_Init(GPIOB, &gpio);
    //GPIO_SetBits(GPIOB, GPIO_Pin_9); 
    GPIO_ResetBits(GPIOB, GPIO_Pin_9); 
 
    regulator_init_default();
    regulator_enable(1);
}

void regulator_enable(int enable)
{
    if (enable) {
        GPIO_SetBits(GPIOB, GPIO_Pin_9); 
    } else {
        GPIO_ResetBits(GPIOB, GPIO_Pin_9); 
    }
}

void regulator_toggle(void)
{
    if (GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9)) {
        GPIO_ResetBits(GPIOB, GPIO_Pin_9); 
    } else {
        GPIO_SetBits(GPIOB, GPIO_Pin_9); 
    }
}

float regulator_get_voltage(void)
{
    return adc_get_voltage(dac_voltage_get_value());
}

float regulator_get_current(void)
{
    return adc_get_current(dac_current_get_value());
}

void regulator_set_voltage(float voltage)
{ 
    dac_voltage_set_value(voltage);
}

void regulator_set_current(float current)
{
    dac_current_set_value(current);
}
