#include "air32f10x.h"

#ifndef _KEY_H_
#define _KEY_H_

typedef enum {
    KEY_MENU,    // KEY_MENU  PB13 菜单
    KEY_OK,      // KEY_OK    PA8  确认
    KEY_UP,      // KEY_UP	  PB14 上
    KEY_DOWN,    // KEY_DOWN  PB4  下
    KEY_LEFT,    // KEY_LEFT  PB5  左
    KEY_RIGHT,   // KEY_RIGHT PB3  右
    KEY_BUTT,
} key_index;

void key_init(void);
uint32_t key_get_counter(key_index index);
uint32_t key_pressed(key_index index);
int key_wait(int timeout);

#endif

