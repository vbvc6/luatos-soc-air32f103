#include "air32f10x.h"

#ifndef _UI_MENU_MAIN_H_
#define _UI_MENU_MAIN_H_

typedef enum {
    UI_MAIN_SCOPE,
    UI_MAIN_MENU,
    UI_MAIN_BUTT,
} ui_main_state;

typedef void (*ui_call_back)(uint32_t *state);

void ui_main_init(void);
void ui_menu_poll(void);
#endif

