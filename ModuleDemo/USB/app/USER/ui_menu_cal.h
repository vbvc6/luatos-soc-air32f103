#include "air32f10x.h"

#ifndef _UI_MENU_CAL_H_
#define _UI_MENU_CAL_H_

int menu_vol_calibration(void *data);
int menu_cur_calibration(void *data);

#endif

