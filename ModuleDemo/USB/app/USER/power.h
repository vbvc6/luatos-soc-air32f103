#include "air32f10x.h"

#ifndef _POWER_H_
#define _POWER_H_

void power_init(void);
void power_down(void);

#endif

