#include <stdlib.h>
#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu_cal.h"
#include "ui_input.h"
#include "dac.h"
#include "regulator.h"
#include "env.h"
#include "env_const.h"

int menu_current_debug(void *data)
{
    ui_input_edit_int("dac", dac_current_get_raw_value(), 65535, dac_current_set_raw_value);    
    return 0;
}

void menu_current_data(int current)
{
    regulator_set_current(current / 1000.0);
}

int menu_current_set(void *data)
{
    char buffer[12];
    int current = 10;

    if (env_get(CUR_DEF, buffer, sizeof(buffer))) {
        current = atoi(buffer);
    }
    ui_input_edit_int("Current(mA)", current, 5000, menu_current_data);    
    return 0;
}

