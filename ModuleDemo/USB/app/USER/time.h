#include <stdint.h>
#include "singlelist.h"

#ifndef _TIME_H_
#define _TIME_H_

typedef void (*time_func)(uint32_t *counter);

typedef struct {
    list *next;
    time_func cb;
    uint32_t counter;
} time_cb;

void time_init(void);
uint32_t time_get(void);
void time_delay(uint32_t ms);
void time_delay_isr(uint32_t ms);
void time_add_cb(time_cb *cb);

#endif

