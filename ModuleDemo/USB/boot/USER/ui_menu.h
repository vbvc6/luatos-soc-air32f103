#include "air32f10x.h"

#ifndef _UI_MENU_H_
#define _UI_MENU_H_

typedef int (*menu_cb)(void *data);

typedef struct {
    const char *title;
    menu_cb cb;
    void *data;
} menu_item;

void ui_menu_entry(const char *menu_title, const menu_item *menus, uint32_t num);

#endif

