#include <string.h>
#include "lcd12864.h"
#include "time.h"
#include "ui_input.h"
#include "display.h"
#include "key.h"

int ui_input_get_digit_num(int max)
{
    int cnt = 0;
    while (max > 0) {
        max = max / 10;
        cnt++;
    }
    if (cnt == 0) {
        cnt = 1;
    }
    return cnt;
}

void ui_input_fill(char *digit, int size, int value)
{
    int scale = 1;
    
    memset(digit, '0', size);
    for (int i = 0; i < size; i++) {
        int num = value / scale;
        if (num == 0) {
            return;
        }
        digit[i] = (num % 10) + '0';
        scale = scale * 10;
    }
}

void ui_input_show_value(int x, int y, int value, int num, int pos)
{
    char digit[12] = {0};
    
    ui_input_fill(digit, sizeof(digit), value);
    for (int i = 0; i < num; i++) {
        display_char(x + 6 * (num - 1 - i), y, digit[i]);
        if (i == pos) {
            display_char(x + 6 * (num - 1 - i), y - 8, '^');
            display_char(x + 6 * (num - 1 - i), y - 12, '^');
        }
    }
}

int ui_input_pow(int pos)
{
    int value = 1;
    
    for (int i = 0; i < pos; i++) {
        value = value * 10;
    }
    return value;
}

int ui_input_data_add(int code, int pos, int max)
{
    int value = code;
    int step = ui_input_pow(pos);
    value = value + step;
    if (value > max) {
        value = max;
    }
    return value;
}

int ui_input_data_sub(int code, int pos)
{
    int value = code;
    int step = ui_input_pow(pos);
    value = value - step;
    if (value < 0) {
        value = 0;
    }
    return value;
}

int ui_input_edit_int(const char *title, int current, int max, intCb cb)
{
    int maxNum = ui_input_get_digit_num(max);
    int startPos = (128 - (6 * maxNum)) / 2;
    int titlePos = (128 - strlen(title) * 6) / 2;
    int code = current;
    int key;
    int codePos = 2;
    
    if (cb) {
        cb(code);
    }
    while (1) {
        lcd12864_clear();
        display_string(titlePos, 56, title);
        ui_input_show_value(startPos, 28, code, maxNum, codePos);
        
        key = key_wait(0);
        if (key == KEY_LEFT) {
            if (codePos < maxNum - 1) {
                codePos++;
            }
        } 
        if (key == KEY_RIGHT) {
            if (codePos > 0) {
                codePos--;
            }
        }
        if (key == KEY_UP) {
            code = ui_input_data_add(code, codePos, max);
            if (cb) {
                cb(code);
            }
        }
        if (key == KEY_DOWN) {
            code = ui_input_data_sub(code, codePos);
            if (cb) {
                cb(code);
            }
        }
        if (key == KEY_OK) {
            return code;
        } 
    }
}
