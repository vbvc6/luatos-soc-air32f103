#include "air32f10x_gpio.h"
#include "air32f10x_exti.h"
#include "key.h"
#include "display.h"
#include "air32f10x_tim.h"
#include "time.h"
#include "power.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

typedef struct {
    GPIO_TypeDef* port;
    uint16_t pin;
} key_info;

const key_info g_keys[KEY_BUTT] = {
    {GPIOB, GPIO_Pin_3},     // KEY_MENU  PB3 �˵�
    {GPIOB, GPIO_Pin_4},     // KEY_OK    PB4  ȷ��
    {GPIOA, GPIO_Pin_15},    // KEY_UP	  PA15 ��
    {GPIOB, GPIO_Pin_6},     // KEY_DOWN  PB6  ��
    {GPIOB, GPIO_Pin_5},     // KEY_LEFT  PB5  ��
    {GPIOB, GPIO_Pin_7},     // KEY_RIGHT PB7  ��
};

uint32_t g_key_cnt[KEY_BUTT];
uint32_t g_key_state[KEY_BUTT];
uint32_t g_key_pressed[KEY_BUTT];
time_cb g_key_auto_poweroff;
TimerHandle_t g_key_timer;

uint32_t key_get_counter(key_index index)
{   
    return g_key_cnt[index];
}

uint32_t key_pressed(key_index index)
{
    uint32_t value = g_key_pressed[index];
    
    g_key_pressed[index] = 0;
    return value;
}

int key_wait(int timeout)
{
    int i;
    uint32_t start = xTaskGetTickCount();
    
    while (1) {
        if (timeout > 0) {
            if ((int)(xTaskGetTickCount() - start) > timeout) {
                return KEY_BUTT;
            }
        }
        for (i = 0; i < KEY_BUTT; i++) {
            if (g_key_pressed[i]) {
                g_key_pressed[i] = 0;
                return i;
            }
        }
        vTaskDelay(10);
    }    
}

void key_poll(TimerHandle_t xTimer)
{
    for (int i = 0; i < KEY_BUTT; i++) {
        uint32_t state = GPIO_ReadInputDataBit(g_keys[i].port, g_keys[i].pin);
        if ((g_key_state[i] != state) && (state != Bit_RESET)) {
            g_key_pressed[i] = 1;
        }
        if (g_key_state[i] == Bit_RESET) {
            g_key_cnt[i]++;
        } else {
            g_key_cnt[i] = 0;
        }
        g_key_state[i] = state;        
    }
}

void key_poll_init(void)
{
    for (int i = 0; i < KEY_BUTT; i++) {
        g_key_state[i] = GPIO_ReadInputDataBit(g_keys[i].port, g_keys[i].pin);
    }
    g_key_timer = xTimerCreate("key_poll", 10, pdTRUE, 0, key_poll);
    xTimerStart(g_key_timer, 10);
}

void key_busy_delay(uint32_t time)
{
    volatile uint32_t value = 0;
    
    for (int i = 0; i < time *10000; i++) {
        value++;
    }
}

void key_init(void)
{
    GPIO_InitTypeDef gpio;

    gpio.GPIO_Pin = GPIO_Pin_15;
    gpio.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &gpio);
    
    gpio.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Init(GPIOB, &gpio);
    
    key_poll_init();  
}




