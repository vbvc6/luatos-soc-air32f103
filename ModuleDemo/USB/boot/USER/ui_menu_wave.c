#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu_update.h"
#include "adc.h"
#include "wave.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define SCALE_NUM ARRAY_SIZE(g_wave_rate_str)

const char *g_wave_rate_str[] = {"0.001s", "0.01s", "0.01s", "0.1s", "1s", "10s"};
const int g_wave_rate[SCALE_NUM] = {100000000 / 32 / 1000, 100000000 / 32 / 100, 100000000 / 32 / 10, 100000000 / 32, 100000000 / 32 * 10, 100000000 / 32 * 100};
int g_wave_sel = 0;

void menu_wave_sample_rate(int key)
{
    if (key  == KEY_LEFT) {
        if (g_wave_sel < (SCALE_NUM - 1)) {
            g_wave_sel++;
            adc_set_period(g_wave_rate[g_wave_sel]);
        }        
    }   
    if (key == KEY_RIGHT) {
        if (g_wave_sel > 0) {
            g_wave_sel--;
            adc_set_period(g_wave_rate[g_wave_sel]);
        }        
    }  
}

void menu_wave_show_scale(void)
{
    display_string(98, 33, g_wave_rate_str[g_wave_sel]);
}

int menu_wave_show_all(void *data)
{
    int key;
    uint16_t voltage[128];
    uint16_t current[128];
    
    while (1) {
        lcd12864_clear();
        display_string(32, 56, "Vol and cur");
        adc_get_realtime_all_wave(voltage, current);
        wave_show_all(voltage, current);
        menu_wave_show_scale();
        key = key_wait(500);
        if (key == KEY_OK) {
            return 0;
        }         
        menu_wave_sample_rate(key);
    }
}

int menu_wave_show_voltage(void *data)
{
    int key;
    uint16_t buffer[128];
    
    while (1) {
        lcd12864_clear();
        display_string(40, 56, "Voltage");
        adc_get_realtime_voltage_wave(buffer);
        wave_show_voltage(buffer);
        menu_wave_show_scale();
        key = key_wait(500);
        if (key  == KEY_OK) {
            return 0;
        }
        menu_wave_sample_rate(key);
    }
}

int menu_wave_show_current(void *data)
{
    int key;
    uint16_t buffer[128];
    
    while (1) {
        lcd12864_clear();
        display_string(40, 56, "Current");
        adc_get_realtime_current_wave(buffer);
        wave_show_current(buffer);
        menu_wave_show_scale();
        key = key_wait(500);
        if (key == KEY_OK) {
            return 0;
        }         
        menu_wave_sample_rate(key);
    }
}
