#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu.h"
#include "ui_menu_cal.h"
#include "ui_input.h"
#include "dac.h"
#include "regulator.h"
#include "env.h"
#include "adc.h"
#include "env_const.h"
#include "fix_voltage.h"

int menu_cali_voltage(void *data)
{
    char buffer[16];
    int index = (int)data;
    int code;

    lcd12864_clear();
    code = ui_input_edit_int("code", fix_voltage_get_dac(index), 4095, dac_voltage_set_raw_value);
    sprintf(buffer, "%d", code);
    env_set((char *)fix_voltage_get_name(index), buffer);
    return 0;
}

int menu_vol_calibration(void *data)
{
    menu_item menus[VOL_BUTT];
    
    for (int i = 0; i < VOL_BUTT; i++) {
        menus[i].title = fix_voltage_get_name((fix_voltage)i);
        menus[i].cb = menu_cali_voltage;
        menus[i].data = (void *)i;
    }
    
    ui_menu_entry("Calibrate Vol", menus, sizeof(menus) / sizeof(menus[0]));     
    return 0;
}

int menu_cur_calibration(void *data)
{
    int key;
    int adc_off = 0;
    char buffer[12];

    while (1) {
        lcd12864_clear();
        display_string(0, 56, "disconnect load");
        display_string(0, 48, "key up to start");
        if (adc_off) {
            display_string(0, 40, "done");
        }
        key = key_wait(0);
        if (key == KEY_OK) {
            return 0;
        }
        if (key == KEY_UP) {
            adc_off = adc_get_current(0);
            sprintf(buffer, "%d", adc_off);
            env_set(ADC_OFFSET, buffer);       
        }
    }  
}

