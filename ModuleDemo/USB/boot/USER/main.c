#include "lcd12864.h"
#include "time.h"
#include "power.h"
#include "display.h"
#include "adc.h"
#include "wave.h"
#include "key.h"
#include "ui.h"
#include "signal.h"
#include "dac.h"
#include "usb.h"
#include "regulator.h"
#include "FreeRTOS.h"
#include "task.h"
#include <stdio.h>

#define ITM_PORT8(n)         (*(volatile unsigned char *)(0xe0000000 + 4*(n)))
#define ITM_PORT16(n)        (*(volatile unsigned short *)(0xe0000000 + 4*(n)))
#define ITM_PORT32(n)        (*(volatile unsigned long *)(0xe0000000 + 4*(n)))
#define DEMCR                (*(volatile unsigned long *)(0xE000EDFC))
#define TRCENA               0X01000000

extern uint32_t __Vectors[];

int fputc(int chr, FILE *stream)
{
    if(DEMCR & TRCENA) {
        while(ITM_PORT32(0) == 0);                                                                                                                                                                                                                                                                                      
        ITM_PORT8(0) = chr;
    }
    return chr;
}

static void task_main(void *pvParameters)
{
    printf("task_main\r\n");
    // time_init();
    lcd12864_init();
    adc_init();
    key_init();
    dac_init();
    ui_init();
    usb_init();
    regulator_init();
    
    while (1) {
        ui_poll();
    }
}

int main()
{
    SCB->VTOR = (uint32_t)__Vectors;
    power_init();
    xTaskCreate( task_main, "task_main", 1024, NULL, 3, NULL );    
	/* Start the scheduler. */
	vTaskStartScheduler();
}
