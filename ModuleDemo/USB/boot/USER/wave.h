#include "lcd12864.h"

#ifndef _WAVE_H_
#define _WAVE_H_

void wave_show_all(uint16_t *voltage, uint16_t *current);
void wave_show_voltage(uint16_t *buffer);
void wave_show_current(uint16_t *buffer);

#endif

