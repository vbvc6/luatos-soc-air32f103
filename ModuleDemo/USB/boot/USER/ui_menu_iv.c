#include "time.h"
#include "display.h"
#include "ui_menu_debug.h"
#include "adc.h"
#include "regulator.h"
#include "key.h"
#include "FreeRTOS.h"
#include "task.h"

int menu_iv_test(void *data)
{
    int key;
    float vol_start = 0, vol_end = 5.0, i_limit = 0.01;
    float step = (vol_end - vol_start) / 128;
    float vol_measure, current_measure;
    int i;

    while (1) {
        lcd12864_clear();
        display_string(40, 56, "IV test");
        regulator_set_voltage(0);    
        regulator_set_current(i_limit);
        vTaskDelay(1000);
        for (i = 0; i < 128; i++) {            
            regulator_set_voltage(i * step);            
            vTaskDelay(2);
            vol_measure = regulator_get_voltage();
            current_measure = regulator_get_current();
            lcd12864_set_point_value(i, vol_measure * 64 / vol_end, 1);
            lcd12864_set_point_value(i, current_measure * 64 / i_limit, 1);
        }        
        key = key_wait(1000);
        if (key  == KEY_OK) {
            return 0;
        }         
    }
}
