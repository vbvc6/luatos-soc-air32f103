#include "lcd12864.h"

#ifndef _DISPLAY_H_
#define _DISPLAY_H_

uint32_t display_char(int x, int y, char chr);
uint32_t display_string(int x, int y, const char *string);
uint32_t display_digit(int x, int y, uint32_t main, uint32_t sub);
uint32_t display_digit_unit(int x, int y, uint32_t main, uint32_t sub, char *step, char *unit);
uint32_t display_float(int x, int y, float value, char *unit);
uint32_t display_int(int x, int y, int value, char *unit);
uint32_t display_measure(int x, int y, int32_t data, char *unit);
    
#endif

