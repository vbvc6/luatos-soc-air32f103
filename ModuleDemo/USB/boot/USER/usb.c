#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"
#include "usb.h"
#include "FreeRTOS.h"
#include "task.h"
void usb_fifo_init(void);

static void task_usb(void *pvParameters)
{
    uint8_t usbstatus=0;
    uint16_t len;
    uint16_t t;
    uint16_t times=0;  
    
	usb_fifo_init();
    vTaskDelay(1000);
    USB_Port_Set(0);
    vTaskDelay(700);
    USB_Port_Set(1);
    Set_USBClock();
    USB_Interrupts_Config();
    USB_Init();
    DP_PUUP = 1;
    
	while(1)
	{
		if (usbstatus != bDeviceState) {
			usbstatus = bDeviceState;
		}
		if (USB_USART_RX_STA & 0x8000) {					   
			len = USB_USART_RX_STA & 0x3FFF;
			for(t = 0; t < len; t++) {
				USB_USART_SendData(USB_USART_RX_BUF[t]);
			}
			usb_printf("\r\n\r\n");
			USB_USART_RX_STA = 0;
		} else {
			times++;
			if(times % 200 == 0) {
                usb_printf("type and with enter\r\n");  
            }
			vTaskDelay(10);   
		}	
	}
}

void usb_init(void)
{
    xTaskCreate( task_usb, "task_usb", 1024, NULL, 4, NULL );    
}
