#include "wave.h"

#define CV_DATA(X) (X >> 6)
void wave_shape(void)
{
    for (int i = 0; i < 128; i++) {
        //lcd12864_set_point(i, 0);
        //lcd12864_set_point(i, 63);
        if ((i & 0x3) == 0) {
            lcd12864_set_point(i, 48);
            lcd12864_set_point(i, 32);
            lcd12864_set_point(i, 16);
        }
    }
    for (int i = 0; i < 64; i++) {
        //lcd12864_set_point(0, i);
        //lcd12864_set_point(127, i);
        if ((i & 0x3) == 0) {
            lcd12864_set_point(32, i);
            lcd12864_set_point(64, i);
            lcd12864_set_point(96, i);
        }
    }
}

void wave_data_current(uint16_t *buffer)
{
    int last = CV_DATA(buffer[0]);
    int cur, start, end;
    
    for (int i = 0;i < 128; i++) {
        cur = CV_DATA(buffer[i]);
        lcd12864_set_point(i, cur);
        if (last != cur) {
            if (last < cur) {
                start = last;
                end = cur;
            } else {
                end = last;
                start = cur;
            }
            for (int j = start + 1; j < end; j++) {
                lcd12864_set_point(i, j);
            }
        }
        last = cur;        
    }
}

void wave_data_voltage(uint16_t *buffer)
{
    int last = CV_DATA(buffer[1]);
    int cur, start, end;
    
    for (int i = 0;i < 128; i++) {
        cur = CV_DATA(buffer[i]);
        lcd12864_set_point(i, cur);
        if (last != cur) {
            if (last < cur) {
                start = last;
                end = cur;
            } else {
                end = last;
                start = cur;
            }
            for (int j = start + 1; j < end; j++) {
                lcd12864_set_point(i, j);
            }
        }
        last = cur;        
    }
}

void wave_show_all(uint16_t *voltage, uint16_t *current)
{
    wave_shape();
    wave_data_voltage(voltage);
    wave_data_current(current);
}

void wave_show_voltage(uint16_t *buffer)
{
    wave_shape();
    wave_data_voltage(buffer);
}

void wave_show_current(uint16_t *buffer)
{
    wave_shape();
    wave_data_current(buffer);
}
