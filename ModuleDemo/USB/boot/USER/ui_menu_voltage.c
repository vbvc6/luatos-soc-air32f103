#include <stdlib.h>
#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu.h"
#include "regulator.h"
#include "ui_menu_voltage.h"
#include "env.h"
#include "ui_input.h"
#include "fix_voltage.h"

int menu_voltage_set_voltage(void *data)
{
    int index = (int)data;
    
    regulator_set_voltage(fix_voltage_get_value(index));
    return 1;
}

int menu_voltage_select(void *data)
{
    menu_item menus[VOL_BUTT];
    
    for (int i = 0; i < VOL_BUTT; i++) {
        menus[i].title = fix_voltage_get_name((fix_voltage)i);
        menus[i].cb = menu_voltage_set_voltage;
        menus[i].data = (void *)i;
    }
    
    ui_menu_entry("Select Vol", menus, sizeof(menus) / sizeof(menus[0]));
    return 0;
}

void menu_voltage_data(int voltage)
{
    regulator_set_voltage(voltage / 1000.0);
}

int menu_voltage_set(void *data)
{
    char buffer[12];
    int voltage = 3300;

    if (env_get(VOL_DEF, buffer, sizeof(buffer))) {
        voltage = atoi(buffer);
    }
    ui_input_edit_int("Voltage(mV)", voltage, 25000, menu_voltage_data);    
    return 0;
}

