#include "time.h"
#include "display.h"
#include "ui_menu_debug.h"
#include "adc.h"
#include "regulator.h"
#include "key.h"

time_cb g_ui_debug;
uint32_t g_ui_last;
volatile uint32_t g_ui_deta;
volatile uint32_t g_ui_time;

int menu_debug_set(void *data)
{
    int key;
    int start;
    
    while (1) {
        lcd12864_clear();

        start = display_string(0, 56, "Adc_c: ");
        display_int(start, 56, adc_get_current(0), "");         

        start = display_string(0, 40, "Current: ");
        display_float(start, 40, regulator_get_current(), "A"); 

        start = display_string(0, 24, "Voltage: ");
        display_float(start, 24, regulator_get_voltage(), "V"); 
        
        key = key_wait(1000);
        if (key  == KEY_OK) {
            return 0;
        }         
    }
}
