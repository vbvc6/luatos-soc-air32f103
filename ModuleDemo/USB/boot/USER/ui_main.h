#include "air32f10x.h"

#ifndef _UI_MAIN_H_
#define _UI_MAIN_H_

void ui_main_init(void);
void ui_main_poll(void);
void ui_scope_show_main(void);

#endif

