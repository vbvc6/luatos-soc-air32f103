#include <string.h>
#include "air32f10x_rcc.h"
#include "air32f10x_dma.h"
#include "air32f10x_tim.h"
#include "adc.h"
#include "dac.h"

#define MAX_DMA_BUFFER 2048
#define MAX_ADC_CHNS 4

volatile uint16_t g_wave_buffer[MAX_DMA_BUFFER];
uint32_t g_adc_period_us;

void adc_set_duty(uint32_t duty)
{
    TIM_TimeBaseInitTypeDef timer;
    TIM_OCInitTypeDef tim_ch;
    uint32_t prescaler, period;
    
    period = duty;
    prescaler = 0;
    if (period > 0xffff) {
        prescaler = period / 0xffff;
        period = period / (prescaler + 1);
    }

    RCC_APB2PeriphResetCmd(RCC_APB2Periph_TIM1, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_TIM1, DISABLE);
    
    timer.TIM_Prescaler = prescaler;
    timer.TIM_CounterMode = TIM_CounterMode_Up;
    timer.TIM_Period = period;
    timer.TIM_ClockDivision = TIM_CKD_DIV1;
    timer.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM1, &timer);
    
    tim_ch.TIM_OCMode = TIM_OCMode_PWM1;
    tim_ch.TIM_OutputState = TIM_OutputState_Enable;
    tim_ch.TIM_OutputNState = TIM_OutputState_Enable;
    tim_ch.TIM_Pulse = 0x1;
    tim_ch.TIM_OCPolarity = TIM_OCPolarity_High;
    tim_ch.TIM_OCNPolarity = TIM_OCPolarity_Low;
    tim_ch.TIM_OCIdleState = TIM_OCIdleState_Set;
    tim_ch.TIM_OCNIdleState = TIM_OCIdleState_Reset;
    TIM_OC1Init(TIM1, &tim_ch);
    TIM_SetCompare1(TIM1, period / 2);
    TIM_CtrlPWMOutputs(TIM1, ENABLE);
    TIM_Cmd(TIM1, ENABLE);
    
    g_adc_period_us = 1000 / 72 * period * prescaler;
}

void adc_set_period(uint32_t ns)
{
#define MINI_PERIOD (1000 * 16 / 12 * 4)
    uint32_t duty;
    
    if (ns < MINI_PERIOD) {
        ns = MINI_PERIOD;
    }
    if (ns > 1000000) {
        duty = ns  / 1000 * 216;
    } else {
        duty = ns * 216 / 1000;
    }
    
    adc_set_duty(duty);
}

uint32_t adc_get_period_ns(void)
{
    return g_adc_period_us;
}

void adc_get_buffer(uint16_t *buffer, uint32_t from, uint32_t cnt)
{
    uint32_t pos = MAX_DMA_BUFFER - from;
    uint32_t start = pos;
    
    if (start >= cnt) {
        start = start - cnt;
    } else {
        start = start - cnt + MAX_DMA_BUFFER;
    }
    if ((start + 128) <= MAX_DMA_BUFFER) {
        memcpy((void *)buffer, (void *)&g_wave_buffer[start], cnt * sizeof(uint16_t));
    } else {
        memcpy((void *)buffer, (void *)&g_wave_buffer[start], (MAX_DMA_BUFFER - start)  * sizeof(uint16_t));        
        memcpy((void *)&buffer[MAX_DMA_BUFFER - start], (void *)&g_wave_buffer[0], (cnt + start - MAX_DMA_BUFFER)  * sizeof(uint16_t));
    }
}

/*
 *   ch6: out+
 *   ch7: out-
 *   ch8: 10mA
 *   ch9: 1A
 */
void adc_init_adc1(void)
{
    ADC_InitTypeDef adc;
    GPIO_InitTypeDef gpio;
    DMA_InitTypeDef dma;
    
    gpio.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    gpio.GPIO_Mode = GPIO_Mode_AIN;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &gpio);
    
    gpio.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_Init(GPIOB, &gpio);
    
    RCC_ADCCLKConfig(RCC_PCLK2_Div6);
    
    adc.ADC_Mode = ADC_Mode_Independent;
    adc.ADC_ScanConvMode = ENABLE;
    adc.ADC_ContinuousConvMode = DISABLE;
    adc.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
    adc.ADC_DataAlign = ADC_DataAlign_Right;
    adc.ADC_NbrOfChannel = MAX_ADC_CHNS;
    ADC_Init(ADC1, &adc);
    ADC_DMACmd(ADC1, ENABLE);
    ADC_Cmd(ADC1, ENABLE);
    
    dma.DMA_PeripheralBaseAddr   = (uint32_t)&ADC1->DR;       
    dma.DMA_MemoryBaseAddr       = (uint32_t)g_wave_buffer; 
    dma.DMA_DIR                  = DMA_DIR_PeripheralSRC;
    dma.DMA_PeripheralInc        = DMA_PeripheralInc_Disable;
    dma.DMA_MemoryInc            = DMA_MemoryInc_Enable;
    dma.DMA_PeripheralDataSize   = DMA_PeripheralDataSize_HalfWord;
    dma.DMA_MemoryDataSize       = DMA_MemoryDataSize_HalfWord;
    dma.DMA_BufferSize           = sizeof(g_wave_buffer) / sizeof(g_wave_buffer[0]);
    dma.DMA_M2M                  = DMA_M2M_Disable;
    dma.DMA_Mode                 = DMA_Mode_Circular;
    dma.DMA_Priority             = DMA_Priority_High;
    DMA_Init(DMA1_Channel1, &dma);   
    DMA_Cmd(DMA1_Channel1, ENABLE);    
    
    ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 1, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_7, 2, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 3, ADC_SampleTime_239Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_9, 4, ADC_SampleTime_239Cycles5);
    
    ADC_Cmd(ADC1,ENABLE);
    ADC_ResetCalibration(ADC1);   
    ADC_ResetCalibration(ADC1);
    while(SET == ADC_GetResetCalibrationStatus(ADC1));
    ADC_StartCalibration(ADC1);
    while(SET == ADC_GetCalibrationStatus(ADC1));

    //ADC_DiscModeChannelCountConfig(ADC1, 1);
    //ADC_DiscModeCmd(ADC1, ENABLE);
    ADC_ExternalTrigConvCmd(ADC1, ENABLE);
    adc_set_period(10000000);
}

void adc_start(void)
{
    TIM_Cmd(TIM1, ENABLE);
}

void adc_stop(void)
{
    TIM_Cmd(TIM1, DISABLE);
}

void adc_init_control(void)
{
    GPIO_InitTypeDef gpio;
    
    gpio.GPIO_Pin = GPIO_Pin_10;
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &gpio);
    GPIO_SetBits(GPIOB, GPIO_Pin_10);
}

void adc_init(void)
{
    adc_init_adc1();
    adc_init_control();
}

static int adc_dma_last(void)
{
    uint32_t dma_pos = MAX_DMA_BUFFER - DMA_GetCurrDataCounter(DMA1_Channel1);

    dma_pos &= (~3);
    dma_pos = MAX_DMA_BUFFER + dma_pos - 4;
    if (dma_pos >= MAX_DMA_BUFFER) {
        dma_pos -= MAX_DMA_BUFFER;
    }
    return dma_pos;
}

float adc_get_voltage(float target)
{
    int index = adc_dma_last();
    return g_wave_buffer[index + 0] * 24.0 / 4095 * (10.96 / 9.18);
}

float adc_get_current(float limit)
{
    int index = adc_dma_last();
    return g_wave_buffer[index + 2] * dac_current_get_max() / 4095;     
}

void adc_get_realtime_voltage_wave(uint16_t *buffer)
{
    uint32_t start = adc_dma_last();
    int i;
    
    if (start >= 128 * MAX_ADC_CHNS) {
        start = start - 128 * MAX_ADC_CHNS;
    } else {
        start = start - 128 * MAX_ADC_CHNS + MAX_DMA_BUFFER;
    }
    for (i = 0; i < 128; i++) {
        if ((start + i * 4) < MAX_DMA_BUFFER) {
            buffer[i] = g_wave_buffer[start + i * 4];
        } else {
            buffer[i] = g_wave_buffer[start + i * 4 - MAX_DMA_BUFFER];
        }
    }
}

void adc_get_realtime_current_wave(uint16_t *buffer)
{
    uint32_t start = adc_dma_last();
    int i;
    
    if (start >= 128 * MAX_ADC_CHNS) {
        start = start - 128 * MAX_ADC_CHNS;
    } else {
        start = start - 128 * MAX_ADC_CHNS + MAX_DMA_BUFFER;
    }
    for (i = 0; i < 128; i++) {
        if ((start + i * 4) < MAX_DMA_BUFFER) {
            buffer[i] = g_wave_buffer[start + i * 4 + 2];
        } else {
            buffer[i] = g_wave_buffer[start + i * 4 + 2 - MAX_DMA_BUFFER];
        }
    }
}

void adc_get_realtime_all_wave(uint16_t *voltage, uint16_t *current)
{
    uint32_t start = adc_dma_last();
    int i;
    
    if (start >= 128 * MAX_ADC_CHNS) {
        start = start - 128 * MAX_ADC_CHNS;
    } else {
        start = start - 128 * MAX_ADC_CHNS + MAX_DMA_BUFFER;
    }
    for (i = 0; i < 128; i++) {
        if ((start + i * 4) < MAX_DMA_BUFFER) {
            voltage[i] = g_wave_buffer[start + i * 4];
            current[i] = g_wave_buffer[start + i * 4 + 2];
        } else {
            voltage[i] = g_wave_buffer[start + i * 4 - MAX_DMA_BUFFER];
            voltage[i] = g_wave_buffer[start + i * 4 + 2 - MAX_DMA_BUFFER];
        }
    }
}

