#include "key.h"
#include "time.h"
#include "lcd12864.h"
#include "display.h"
#include "ui_menu_main.h"
#include "ui_main.h"
#include "ui_menu.h"

void ui_menu_entry(const char *menu_title, const menu_item *menus, uint32_t num)
{
    int i, select = 0;
    int key;
    int max_len = num > 7 ? 7 : num;
    int index;
    
    while (1) {
        lcd12864_clear();
        
        display_string(0, 56, menu_title);
        for (i = 0; i < max_len; i++) {
            index = select > 6 ? select - 6 + i : i;
            if (select == index) {
                display_string(0, 48 - 8 * i, "->");
            }
            display_string(16, 48 - 8 * i, menus[index].title);
        }
        key = key_wait(0);
        if (key == KEY_UP) {
            if (select > 0) {
                select--;
            }
        }
        if (key == KEY_DOWN) {
            if (select < (num - 1)) {
                select++;
            }
        }
        if (key == KEY_MENU) {
            return;
        }
        if (key == KEY_OK) {
            if (menus[select].cb(menus[select].data)) {
                return;
            }
        }        
    }
}

