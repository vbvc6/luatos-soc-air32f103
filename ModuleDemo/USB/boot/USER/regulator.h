#include "air32f10x.h"

#ifndef _REGULATOR_H_
#define _REGULATOR_H_

void regulator_init(void);
void regulator_enable(int enable);
void regulator_toggle(void);
void regulator_set_voltage(float voltage);
void regulator_set_current(float current);
float regulator_get_voltage(void);
float regulator_get_current(void);
float regulator_get_input(void);
float regulator_get_ocp(void);
float fix_voltage_get_value(int index);
const char *fix_voltage_get_name(int index);
int fix_voltage_get_dac(int index);

#endif

