#include "lcd12864.h"
#include "air32f10x_gpio.h"
#include "air32f10x_spi.h"
#include "air32f10x_dma.h"
#include "time.h"
#include "string.h"
#include "FreeRTOS.h"
#include "task.h"

//#define LCD_LIGHT PB8
//#define LCD_RST   PB2
//#define LCD_RW    PC14
//#define LCD_DC    PC15
//#define LCD_CS    PA4
//#define LCD_CLK   PA5
//#define LCD_MOSI  PA7


// COMMANDS
#define BLACK 1
#define WHITE 0

#define CMD_DISPLAY_OFF   0xAE
#define CMD_DISPLAY_ON    0xAF

#define CMD_SET_DISP_START_LINE  0x40
#define CMD_SET_PAGE  0xB0

#define CMD_SET_COLUMN_UPPER  0x10
#define CMD_SET_COLUMN_LOWER  0x00

#define CMD_SET_ADC_NORMAL  0xA0
#define CMD_SET_ADC_REVERSE 0xA1

#define CMD_SET_DISP_NORMAL 0xA6
#define CMD_SET_DISP_REVERSE 0xA7

#define CMD_SET_ALLPTS_NORMAL 0xA4
#define CMD_SET_ALLPTS_ON  0xA5
#define CMD_SET_BIAS_9 0xA2
#define CMD_SET_BIAS_7 0xA3

#define CMD_RMW  0xE0
#define CMD_RMW_CLEAR 0xEE
#define CMD_INTERNAL_RESET  0xE2
#define CMD_SET_COM_NORMAL  0xC0
#define CMD_SET_COM_REVERSE  0xC8
#define CMD_SET_POWER_CONTROL  0x28
#define CMD_SET_RESISTOR_RATIO  0x20
#define CMD_SET_VOLUME_FIRST  0x81
#define CMD_SET_VOLUME_SECOND  0
#define CMD_SET_STATIC_OFF  0xAC
#define CMD_SET_STATIC_ON  0xAD
#define CMD_SET_STATIC_REG  0x0
#define CMD_SET_BOOSTER_FIRST  0xF8
#define CMD_SET_BOOSTER_234  0
#define CMD_SET_BOOSTER_5  1
#define CMD_SET_BOOSTER_6  3
#define CMD_NOP  0xE3
#define CMD_TEST  0xF0

#define LCDWIDTH 		128
#define LCDHEIGHT 		32

#define LCD_CMD()  GPIO_ResetBits(GPIOB, GPIO_Pin_14);
#define LCD_DATA() GPIO_SetBits(GPIOB, GPIO_Pin_14);

uint8_t g_lcd12864_fb[132*64 / 8];
uint32_t g_lcd12864_backlight;

void lcd12864_init_dev(void)
{
    GPIO_InitTypeDef gpio;
    SPI_InitTypeDef spi;    
   
    gpio.GPIO_Mode = GPIO_Mode_Out_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    
    gpio.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_2 | GPIO_Pin_8 | GPIO_Pin_12; // pb2:rst pb14:dc pb8:light pb12:cs
    GPIO_Init(GPIOB, &gpio);
    
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    gpio.GPIO_Pin = GPIO_Pin_15 | GPIO_Pin_13;  // pb13:clk pb15:mosi
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &gpio);
    
    
    SPI_StructInit(&spi);
    spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
    spi.SPI_Mode = SPI_Mode_Master;
    spi.SPI_Direction = SPI_Direction_1Line_Tx;
    spi.SPI_NSS = SPI_NSS_Soft;
    SPI_Init(SPI2, &spi);
    SPI_Cmd(SPI2, ENABLE);
    
    GPIO_SetBits(GPIOB, GPIO_Pin_12);   // cs = 1    
    vTaskDelay(10);
    GPIO_SetBits(GPIOB, GPIO_Pin_2);    // rst 1
    vTaskDelay(10);
    GPIO_ResetBits(GPIOB, GPIO_Pin_8);  // backlight on
    GPIO_ResetBits(GPIOB, GPIO_Pin_12); // cs = 0    
}

void lcd12864_send(uint8_t data)
{
    SPI_I2S_SendData(SPI2, data);
    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == 0) {
    };
    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) != 0) {
    };
}

void lcd12864_cmd(uint8_t cmd)
{
    //time_delay(2);
    LCD_CMD();
    lcd12864_send(cmd);
    //time_delay(2);
}

void lcd12864_data(uint8_t data)
{
    LCD_DATA();
    lcd12864_send(data);
}

void lcd12864_update(void)
{
    for(uint32_t i = 0; i < sizeof(g_lcd12864_fb); i++){
        lcd12864_data(g_lcd12864_fb[i]);
    }
}

void lcd12864_set_point_value(int x, int y, int value)
{
    if ((x < 0) || (x > 127) || (y < 0) || (y > 63)) {
        return;
    }
    int index = (y >> 3) * 132 + x;
    int bit = y & 0x7;
    
    if (value) {
        g_lcd12864_fb[index] = g_lcd12864_fb[index] | (1 << bit);
    } else {
        g_lcd12864_fb[index] = g_lcd12864_fb[index] & (~(1 << bit));
    }
}

void lcd12864_set_point(int x, int y)
{
    lcd12864_set_point_value(x, y, 1);
}

void lcd12864_clear_point(int x, int y)
{
    lcd12864_set_point_value(x, y, 0);
}

void lcd12864_back_light(int en)
{
    if (en) {
        GPIO_SetBits(GPIOB, GPIO_Pin_8);    // backlight on
        g_lcd12864_backlight = 1;
    } else {
        GPIO_ResetBits(GPIOB, GPIO_Pin_8);    // backlight on
        g_lcd12864_backlight = 0;
    }
}

void lcd12864_back_light_toggle(void)
{
    if (g_lcd12864_backlight == 0) {
        g_lcd12864_backlight = 1;        
    } else {
        g_lcd12864_backlight = 0;
    }
    lcd12864_back_light(g_lcd12864_backlight);
}

void lcd12864_start_dma(void)
{
    DMA_InitTypeDef dma;
    
    dma.DMA_PeripheralBaseAddr = (uint32_t)&SPI2->DR;
    dma.DMA_MemoryBaseAddr = (uint32_t)g_lcd12864_fb;
    dma.DMA_DIR = DMA_DIR_PeripheralDST;
    dma.DMA_BufferSize = sizeof(g_lcd12864_fb);
    dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
    dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    dma.DMA_Mode = DMA_Mode_Circular;
    dma.DMA_Priority = DMA_Priority_Low;
    dma.DMA_M2M = DMA_M2M_Disable;

    lcd12864_cmd(CMD_DISPLAY_ON);
    lcd12864_cmd(CMD_SET_PAGE | 0);
    lcd12864_cmd(CMD_SET_COLUMN_LOWER | (0x00 & 0xf));
    lcd12864_cmd(CMD_SET_COLUMN_UPPER | ((0x00 >> 4) & 0xf));
    LCD_DATA();
    
    DMA_Init(DMA1_Channel5, &dma);
    DMA_Cmd(DMA1_Channel5, ENABLE);
    SPI_I2S_DMACmd(SPI2, SPI_I2S_DMAReq_Tx, ENABLE);
}

void lcd12864_init(void)
{
    lcd12864_init_dev();
    lcd12864_start_dma();
}

void lcd12864_clear(void)
{
    memset((void *)g_lcd12864_fb, 0, sizeof(g_lcd12864_fb));
}



