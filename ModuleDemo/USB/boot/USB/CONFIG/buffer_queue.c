#include <string.h>
#include <stdlib.h>
#include "buffer_queue.h"
#include "FreeRTOS.h"

struct buffer_item {
	uint8_t *buffer;
	uint32_t length;
};

struct buffer_queue {
	uint32_t read;
	uint32_t write;
	uint32_t num;
	uint32_t size;
	struct buffer_item *items;
};

#define malloc pvPortMalloc
#define free vPortFree

void *buffer_queue_create(uint32_t num, uint32_t size)
{
	struct buffer_queue *queue = (struct buffer_queue *)malloc(sizeof(struct buffer_queue));
	
	queue->read = 0;
	queue->write = 0;
	queue->num = num;
	queue->size = size;
	queue->items = (struct buffer_item *)malloc(sizeof(struct buffer_item) * num);
	for (uint32_t i = 0; i < num; i++) {
		queue->items[i].buffer = (uint8_t *)malloc(size);
		queue->items[i].length = 0;
	}
	return queue;
}

void buffer_queue_distroy(void *queue)
{
	struct buffer_queue *buffer_queue = queue;

	for (uint32_t i = 0; i < buffer_queue->num; i++) {
		free(buffer_queue->items[i].buffer);
	}
	free(buffer_queue->items);
	free(queue);
}

uint32_t buffer_queue_count(void *queue)
{
	struct buffer_queue *buffer_queue = queue;
	uint32_t count;
	
	count = buffer_queue->num + buffer_queue->write - buffer_queue->read;
	if (count >= buffer_queue->num) {
		count -= buffer_queue->num;
	}
	return count;
}

int buffer_queue_full(void *queue)
{
	struct buffer_queue *buffer_queue = queue;
	
	if (buffer_queue_count(queue) >= buffer_queue->num - 1) {
		return 1;
	}
	return 0;
}

int buffer_queue_empty(void *queue)
{
	return buffer_queue_count(queue) == 0;
}

uint8_t* buffer_queue_head(void *queue)
{
	struct buffer_queue *buffer_queue = queue;

	return buffer_queue->items[buffer_queue->read].buffer;
}

uint32_t buffer_queue_head_length(void *queue)
{
	struct buffer_queue *buffer_queue = queue;

	return buffer_queue->items[buffer_queue->read].length;
}

void buffer_queue_remove(void *queue)
{
	struct buffer_queue *buffer_queue = queue;
	
	uint32_t next = buffer_queue->read + 1;
	if (next >= buffer_queue->num) {
		next = 0;
	}
	buffer_queue->read = next;
}

void buffer_queue_remove_ext(void *queue, uint8_t* buffer, uint32_t* size)
{
	struct buffer_queue *buffer_queue = queue;
	struct buffer_item *cur = &buffer_queue->items[buffer_queue->read];

	memcpy(buffer, cur->buffer, cur->length);
	*size = cur->length;
	uint32_t next = buffer_queue->read + 1;
	if (next >= buffer_queue->num) {
		next = 0;
	}
	buffer_queue->read = next;
}

uint8_t* buffer_queue_tail_buffer(void *queue)
{
	struct buffer_queue *buffer_queue = queue;

	return buffer_queue->items[buffer_queue->write % buffer_queue->num].buffer;
}

void buffer_queue_add(void *queue, uint32_t size)
{
	struct buffer_queue *buffer_queue = queue;
	struct buffer_item *cur = &buffer_queue->items[buffer_queue->write];

	cur->length = size;
	uint32_t next = buffer_queue->write + 1;
	if (next >= buffer_queue->num) {
		next = 0;
	}
	buffer_queue->write = next;
}

void buffer_queue_add_ext(void *queue, uint8_t* buffer, uint32_t size)
{
	struct buffer_queue *buffer_queue = queue;
	struct buffer_item *cur = &buffer_queue->items[buffer_queue->write];

	memcpy(cur->buffer, buffer, size);
	cur->length = size;
	uint32_t next = buffer_queue->write + 1;
	if (next >= buffer_queue->num) {
		next = 0;
	}
	buffer_queue->write = next;
}

void buffer_queue_clear(void *queue)
{
	struct buffer_queue *buffer_queue = queue;

	buffer_queue->read = 0;
	buffer_queue->write = 0;
}
