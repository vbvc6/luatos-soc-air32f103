#ifndef RNDIS_INTERFACE_H
#define RNDIS_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "rndis_protocol.h"

#define RNDIS_MTU        1500                           /* MTU value */
#define RNDIS_LINK_SPEED 100000000                       /* Link baudrate (12Mbit/s for USB-FS) */
#define RNDIS_VENDOR     "fetisov"                      /* NIC vendor name */
#define RNDIS_HWADDR     MAC0,MAC1,MAC2,MAC3,MAC4,MAC5  /* MAC-address to set to host interface */

#define ETH_HEADER_SIZE             14
#define ETH_MAX_PACKET_SIZE         ETH_HEADER_SIZE + RNDIS_MTU
#define ETH_MIN_PACKET_SIZE         60
#define RNDIS_RX_BUFFER_SIZE        (ETH_MAX_PACKET_SIZE + sizeof(rndis_data_packet_t))
#define USB_CDC_DATA_SIZE 512

#define MAC0 0x20
#define MAC1 0x89
#define MAC2 0x84
#define MAC3 0x6A
#define MAC4 0x96
#define MAC5 0xAB

char *rndis_lwip_get_gateway(char *buf, size_t buflen);
char *rndis_lwip_get_ip_address(char *buf, size_t buflen);
char *rndis_lwip_get_netmask(char *buf, size_t buflen);
void TCPIP_Init(void);
void usb_rndis_write_raw(uint8_t *buffer, uint32_t size);
void ethernetif_input(uint8_t *buffer, uint32_t size);
void usb_rndis_write_raw(uint8_t *buffer, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif
