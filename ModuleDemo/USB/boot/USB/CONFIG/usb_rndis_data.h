#ifndef USB_RNDIS_CTRL_DATA_H
#define USB_RNDIS_CTRL_DATA_H

/* These headers are included for child class. */
#include "usb_rndis_ctrl.h"

void writePacket(uint8_t *buffer, uint32_t size);
void PacketArrive(uint8_t *buffer, uint32_t size);
void processData(void); 

#endif
