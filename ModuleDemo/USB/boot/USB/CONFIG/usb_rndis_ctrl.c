#include "usb_rndis_ctrl.h"
#include "rndis_interface.h"
#include "rndis_protocol.h"

static const uint8_t station_hwaddr[6] = { RNDIS_HWADDR };
static const uint8_t permanent_hwaddr[6] = { RNDIS_HWADDR };
static const char *rndis_vendor = RNDIS_VENDOR;
uint32_t oid_packet_filter = 0x0000000;
usb_eth_stat_t usb_eth_stat = { 0, 0, 0, 0 };
bool _init_done;
enum rnids_state_e _state;

const uint32_t OIDSupportedList[] = 
{
    OID_GEN_SUPPORTED_LIST,
    OID_GEN_HARDWARE_STATUS,
    OID_GEN_MEDIA_SUPPORTED,
    OID_GEN_MEDIA_IN_USE,
    OID_GEN_MAXIMUM_FRAME_SIZE,
    OID_GEN_LINK_SPEED,
    OID_GEN_TRANSMIT_BLOCK_SIZE,
    OID_GEN_RECEIVE_BLOCK_SIZE,
    OID_GEN_VENDOR_ID,
    OID_GEN_VENDOR_DESCRIPTION,
    OID_GEN_VENDOR_DRIVER_VERSION,
    OID_GEN_CURRENT_PACKET_FILTER,
    OID_GEN_MAXIMUM_TOTAL_SIZE,
    OID_GEN_PROTOCOL_OPTIONS,
    OID_GEN_MAC_OPTIONS,
    OID_GEN_MEDIA_CONNECT_STATUS,
    OID_GEN_MAXIMUM_SEND_PACKETS,
    OID_802_3_PERMANENT_ADDRESS,
    OID_802_3_CURRENT_ADDRESS,
    OID_802_3_MULTICAST_LIST,
    OID_802_3_MAXIMUM_LIST_SIZE,
    OID_802_3_MAC_OPTIONS
};

#define OID_LIST_LENGTH (sizeof(OIDSupportedList) / sizeof(*OIDSupportedList))
#define ENC_BUF_SIZE    (OID_LIST_LENGTH * 4 + 32)

#define INFBUF ((uint32_t *)((uint8_t *)&(m->RequestId) + m->InformationBufferOffset))
#define CFGBUF ((rndis_config_parameter_t *) INFBUF)
#define PARMNAME  ((uint8_t *)CFGBUF + CFGBUF->ParameterNameOffset)
#define PARMVALUE ((uint8_t *)CFGBUF + CFGBUF->ParameterValueOffset)
#define PARMVALUELENGTH	CFGBUF->ParameterValueLength
#define PARM_NAME_LENGTH 25 /* Maximum parameter name length */

void rndis_query_cmplt(int status, const void *data, int size,uint8_t *rndis_msg_buffer);
void rndis_query_cmplt32(int status, uint32_t data, uint8_t *rndis_msg_buffer);
void rndis_query(uint8_t *rndis_msg_buffer);
void rndis_handle_config_parm(const char *data, int keyoffset, int valoffset, int keylen, int vallen);
void rndis_packetFilter(uint32_t newfilter);
void rndis_handle_set_msg(uint8_t *rndis_msg_buffer);

void rndis_query_cmplt(int status, const void *data, int size,uint8_t *rndis_msg_buffer)
{
	rndis_query_cmplt_t *c;

	c = (rndis_query_cmplt_t *)rndis_msg_buffer;
	c->MessageType = REMOTE_NDIS_QUERY_CMPLT;
	c->MessageLength = sizeof(rndis_query_cmplt_t) + size;
	c->InformationBufferLength = size;
	c->InformationBufferOffset = 16;
	c->Status = status;
	memcpy(c + 1, data, size);
}

void rndis_query_cmplt32(int status, uint32_t data, uint8_t *rndis_msg_buffer)
{
	rndis_query_cmplt_t *c;

	c = (rndis_query_cmplt_t *)rndis_msg_buffer;
	c->MessageType = REMOTE_NDIS_QUERY_CMPLT;
	c->MessageLength = sizeof(rndis_query_cmplt_t) + 4;
	c->InformationBufferLength = 4;
	c->InformationBufferOffset = 16;
	c->Status = status;
	*(uint32_t *)(c + 1) = data;
}

void rndis_query(uint8_t *rndis_msg_buffer)
{
	switch (((rndis_query_msg_t *)rndis_msg_buffer)->Oid)
	{
		case OID_GEN_SUPPORTED_LIST:         rndis_query_cmplt(RNDIS_STATUS_SUCCESS, OIDSupportedList, 4 * OID_LIST_LENGTH,rndis_msg_buffer); return;
		case OID_GEN_VENDOR_DRIVER_VERSION:  rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0x00001000,rndis_msg_buffer);  return;
		case OID_802_3_CURRENT_ADDRESS:      rndis_query_cmplt(RNDIS_STATUS_SUCCESS, &station_hwaddr, 6,rndis_msg_buffer); return;
		case OID_802_3_PERMANENT_ADDRESS:    rndis_query_cmplt(RNDIS_STATUS_SUCCESS, &permanent_hwaddr, 6,rndis_msg_buffer); return;
		case OID_GEN_MEDIA_SUPPORTED:        rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, NDIS_MEDIUM_802_3,rndis_msg_buffer); return;
		case OID_GEN_MEDIA_IN_USE:           rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, NDIS_MEDIUM_802_3,rndis_msg_buffer); return;
		case OID_GEN_PHYSICAL_MEDIUM:        rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, NDIS_MEDIUM_802_3,rndis_msg_buffer); return;
		case OID_GEN_HARDWARE_STATUS:        rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		case OID_GEN_LINK_SPEED:             rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, RNDIS_LINK_SPEED / 100,rndis_msg_buffer); return;
		case OID_GEN_VENDOR_ID:              rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0x00FFFFFF,rndis_msg_buffer); return;
		case OID_GEN_VENDOR_DESCRIPTION:     rndis_query_cmplt(RNDIS_STATUS_SUCCESS, rndis_vendor, strlen(rndis_vendor) + 1,rndis_msg_buffer); return;
		case OID_GEN_CURRENT_PACKET_FILTER:  rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, oid_packet_filter,rndis_msg_buffer); return;
		case OID_GEN_MAXIMUM_FRAME_SIZE:     rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, ETH_MAX_PACKET_SIZE - ETH_HEADER_SIZE,rndis_msg_buffer); return;
		case OID_GEN_MAXIMUM_TOTAL_SIZE:     rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, ETH_MAX_PACKET_SIZE,rndis_msg_buffer); return;
		case OID_GEN_TRANSMIT_BLOCK_SIZE:    rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, ETH_MAX_PACKET_SIZE,rndis_msg_buffer); return;
		case OID_GEN_RECEIVE_BLOCK_SIZE:     rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, ETH_MAX_PACKET_SIZE,rndis_msg_buffer); return;
		case OID_GEN_MEDIA_CONNECT_STATUS:   rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, NDIS_MEDIA_STATE_CONNECTED,rndis_msg_buffer); return;
		case OID_GEN_RNDIS_CONFIG_PARAMETER: rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		case OID_802_3_MAXIMUM_LIST_SIZE:    rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 1,rndis_msg_buffer); return;
		case OID_802_3_MULTICAST_LIST:       rndis_query_cmplt32(RNDIS_STATUS_NOT_SUPPORTED, 0,rndis_msg_buffer); return;
		case OID_802_3_MAC_OPTIONS:          rndis_query_cmplt32(RNDIS_STATUS_NOT_SUPPORTED, 0,rndis_msg_buffer); return;
		case OID_GEN_MAC_OPTIONS:            rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, /*MAC_OPT*/ 0,rndis_msg_buffer); return;
		case OID_802_3_RCV_ERROR_ALIGNMENT:  rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		case OID_802_3_XMIT_ONE_COLLISION:   rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		case OID_802_3_XMIT_MORE_COLLISIONS: rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		case OID_GEN_XMIT_OK:                rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, usb_eth_stat.txok,rndis_msg_buffer); return;
		case OID_GEN_RCV_OK:                 rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, usb_eth_stat.rxok,rndis_msg_buffer); return;
		case OID_GEN_RCV_ERROR:              rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, usb_eth_stat.rxbad,rndis_msg_buffer); return;
		case OID_GEN_XMIT_ERROR:             rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, usb_eth_stat.txbad,rndis_msg_buffer); return;
		case OID_GEN_RCV_NO_BUFFER:          rndis_query_cmplt32(RNDIS_STATUS_SUCCESS, 0,rndis_msg_buffer); return;
		default:                             rndis_query_cmplt(RNDIS_STATUS_FAILURE, NULL, 0,rndis_msg_buffer); return;
	}	
}

void rndis_handle_config_parm(const char *data, int keyoffset, int valoffset, int keylen, int vallen)
{
    (void)data;
    (void)keyoffset;
    (void)valoffset;
    (void)keylen;
    (void)vallen;
}

void rndis_packetFilter(uint32_t newfilter)
{
    (void)newfilter;
}

void rndis_handle_set_msg(uint8_t *rndis_msg_buffer)
{
	rndis_set_cmplt_t *c;
	rndis_set_msg_t *m;
	rndis_Oid_t oid;
	
	c = (rndis_set_cmplt_t *)rndis_msg_buffer;
	m = (rndis_set_msg_t *)rndis_msg_buffer;

	oid = m->Oid;
	c->MessageType = REMOTE_NDIS_SET_CMPLT;
	c->MessageLength = sizeof(rndis_set_cmplt_t);
	c->Status = RNDIS_STATUS_SUCCESS;

	switch (oid)
	{
		/* Parameters set up in 'Advanced' tab */
		case OID_GEN_RNDIS_CONFIG_PARAMETER:
			{
                rndis_config_parameter_t *p;
				char *ptr = (char *)m;
				ptr += sizeof(rndis_generic_msg_t);
				ptr += m->InformationBufferOffset;
				p = (rndis_config_parameter_t *)ptr;
				rndis_handle_config_parm(ptr, p->ParameterNameOffset, p->ParameterValueOffset, p->ParameterNameLength, p->ParameterValueLength);
			}
			break;

		/* Mandatory general OIDs */
		case OID_GEN_CURRENT_PACKET_FILTER:
			oid_packet_filter = *INFBUF;
			if (oid_packet_filter)
			{
				rndis_packetFilter(oid_packet_filter);
				_state = rndis_data_initialized;
			} 
			else 
			{
				_state = rndis_initialized;
			}
			break;

		case OID_GEN_CURRENT_LOOKAHEAD:
			break;

		case OID_GEN_PROTOCOL_OPTIONS:
			break;

		/* Mandatory 802_3 OIDs */
		case OID_802_3_MULTICAST_LIST:
			break;

		/* Power Managment: fails for now */
		case OID_PNP_ADD_WAKE_UP_PATTERN:
		case OID_PNP_REMOVE_WAKE_UP_PATTERN:
		case OID_PNP_ENABLE_WAKE_UP:
		default:
			c->Status = RNDIS_STATUS_FAILURE;
			break;
	}
	
	//rndis_ctrl_write(sizeof(rndis_set_cmplt_t));	
	//printf("REMOTE_NDIS_SET_MSG 0x%x\r\n", oid);
	return;
}

void processCtrl(uint8_t *buffer, uint32_t size)
{
	uint32_t Oid = ((rndis_query_msg_t *)buffer)->Oid;

	switch (((rndis_generic_msg_t *)buffer)->MessageType)
	{
		case REMOTE_NDIS_INITIALIZE_MSG:
			{
				rndis_initialize_cmplt_t *m;
				m = ((rndis_initialize_cmplt_t *)buffer);
				/* m->MessageID is same as before */
				m->MessageType = REMOTE_NDIS_INITIALIZE_CMPLT;
				m->MessageLength = sizeof(rndis_initialize_cmplt_t);
				m->MajorVersion = RNDIS_MAJOR_VERSION;
				m->MinorVersion = RNDIS_MINOR_VERSION;
				m->Status = RNDIS_STATUS_SUCCESS;
				m->DeviceFlags = RNDIS_DF_CONNECTIONLESS;
				m->Medium = RNDIS_MEDIUM_802_3;
				m->MaxPacketsPerTransfer = 1;
				m->MaxTransferSize = RNDIS_RX_BUFFER_SIZE;
				m->PacketAlignmentFactor = 0;
				m->AfListOffset = 0;
				m->AfListSize = 0;
				//rndis_ctrl_write(sizeof(rndis_initialize_cmplt_t));
				_state = rndis_initialized;		
				//printf("REMOTE_NDIS_INITIALIZE_MSG\r\n");
			}
			break;

		case REMOTE_NDIS_QUERY_MSG:
			rndis_query(buffer);
			//printf("REMOTE_NDIS_QUERY_MSG 0x%x\r\n", Oid);
			if (Oid == OID_GEN_MAXIMUM_TOTAL_SIZE) {
				if (_init_done == false) {	
					_init_done = true;
				}
			}			

			break;
			
		case REMOTE_NDIS_SET_MSG:
			rndis_handle_set_msg(buffer);
			//printf("REMOTE_NDIS_SET_MSG\r\n");
			break;

		case REMOTE_NDIS_RESET_MSG:
			{
				rndis_reset_cmplt_t * m;
				m = ((rndis_reset_cmplt_t *)buffer);
				_state = rndis_uninitialized;
				m->MessageType = REMOTE_NDIS_RESET_CMPLT;
				m->MessageLength = sizeof(rndis_reset_cmplt_t);
				m->Status = RNDIS_STATUS_SUCCESS;
				m->AddressingReset = 1; /* Make it look like we did something */
				//rndis_ctrl_write(sizeof(rndis_reset_cmplt_t));
				//printf("REMOTE_NDIS_RESET_MSG\r\n");
			}
			break;

		case REMOTE_NDIS_KEEPALIVE_MSG:
			{
				rndis_keepalive_cmplt_t * m;
				m = (rndis_keepalive_cmplt_t *)buffer;
				m->MessageType = REMOTE_NDIS_KEEPALIVE_CMPLT;
				m->MessageLength = sizeof(rndis_keepalive_cmplt_t);
				m->Status = RNDIS_STATUS_SUCCESS;
				//rndis_ctrl_write(sizeof(rndis_keepalive_cmplt_t));
				//printf("REMOTE_NDIS_KEEPALIVE_MSG\r\n");
			}
			break;

		default:
			//printf("default\r\n");
			break;
	}
}

bool ready()
{
    // int time = (int)us_ticker_read();
    // printf("ready->[%d.%03d.%03d] ready=%d\r\n", time / 1000000, (time / 1000) % 1000, time % 1000, _state);
	return _init_done;
}
