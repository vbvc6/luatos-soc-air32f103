#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef BUFFER_QUEUE_H
#define BUFFER_QUEUE_H

#include <stdint.h>

void *buffer_queue_create(uint32_t num, uint32_t size);
void buffer_queue_distroy(void *queue);
uint32_t buffer_queue_count(void *queue);
int buffer_queue_full(void *queue);
int buffer_queue_empty(void *queue);

uint8_t* buffer_queue_head(void *queue);
uint32_t buffer_queue_head_length(void *queue);
void buffer_queue_remove(void *queue);
void buffer_queue_remove_ext(void *queue, uint8_t *buffer, uint32_t *size);

uint8_t* buffer_queue_tail(void *queue);
void buffer_queue_add(void *queue, uint32_t size);
void buffer_queue_add_ext(void *queue, uint8_t* buffer, uint32_t size);

void buffer_queue_clear(void *queue);

#endif // !BUFFER_QUEUE_H
