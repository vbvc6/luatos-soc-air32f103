/**
  ******************************************************************************
  * @file    usb_endp.c
  * @author  MCD Application Team
  * @version V4.0.0
  * @date    21-January-2013
  * @brief   Endpoint routines
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_mem.h"
#include "hw_config.h"
#include "usb_istr.h"
#include "usb_pwr.h"
#include "buffer_queue.h"
#include "rndis_interface.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t g_usb_rx_buffer[VIRTUAL_COM_PORT_DATA_SIZE];
uint8_t g_usb_rndis_buffer[RNDIS_RX_BUFFER_SIZE];
uint32_t g_usb_rndis_buffer_cnt;
void *g_ep1_fifo;
int g_ep1_idle;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void usb_fifo_init(void)
{
	g_ep1_fifo = buffer_queue_create(64, 64);
	g_ep1_idle = 1;
}

/*******************************************************************************
* Function Name  : EP1_IN_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_IN_Callback (void)
{
	if (buffer_queue_empty(g_ep1_fifo)) {
		g_ep1_idle = 1;
		return;
	}
	g_ep1_idle = 0;
	uint32_t len = buffer_queue_head_length(g_ep1_fifo);
	UserToPMABufferCopy(buffer_queue_head(g_ep1_fifo), ENDP1_TXADDR, len);
	SetEPTxCount(ENDP1, len);
	SetEPTxValid(ENDP1);   
	buffer_queue_remove(g_ep1_fifo);
}

void usb_rndis_write_raw(uint8_t *buffer, uint32_t size)
{
  buffer_queue_add_ext(g_ep1_fifo, buffer, size);
  if (g_ep1_idle == 1) {
    g_ep1_idle = 0;

    uint32_t len = buffer_queue_head_length(g_ep1_fifo);
    UserToPMABufferCopy(buffer_queue_head(g_ep1_fifo), ENDP1_TXADDR, len);
    SetEPTxCount(ENDP1, len);
    SetEPTxValid(ENDP1);   
    buffer_queue_remove(g_ep1_fifo);
  }
}

/*******************************************************************************
* Function Name  : EP3_OUT_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP3_OUT_Callback(void)
{
	u16 cnt = USB_SIL_Read(EP3_OUT, &g_usb_rndis_buffer[g_usb_rndis_buffer_cnt]);	//得到USB接收到的数据及其长度  
	SetEPRxValid(ENDP3);								//时能端点3的数据接收
  g_usb_rndis_buffer_cnt = g_usb_rndis_buffer_cnt + cnt;
  if ((cnt != VIRTUAL_COM_PORT_DATA_SIZE) || (g_usb_rndis_buffer_cnt >= RNDIS_RX_BUFFER_SIZE)) {
    ethernetif_input(g_usb_rndis_buffer, g_usb_rndis_buffer_cnt);
  }  
}

/*******************************************************************************
* Function Name  : SOF_Callback / INTR_SOFINTR_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void SOF_Callback(void)
{
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

