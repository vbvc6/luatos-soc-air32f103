#include <stdint.h>

#ifndef USB_RNDIS_CTRL_H
#define USB_RNDIS_CTRL_H

void processCtrl(uint8_t *buffer, uint32_t size);

#endif
