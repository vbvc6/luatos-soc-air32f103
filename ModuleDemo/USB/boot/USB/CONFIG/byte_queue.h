#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef BYTE_QUEUE_H
#define BYTE_QUEUE_H

#include <stdint.h>

void *byte_queue_create(uint32_t num);
void byte_queue_distroy(void *queue);
uint32_t byte_queue_count(void *queue);
int byte_queue_full(void *queue);
int byte_queue_empty(void *queue);

uint8_t byte_queue_read(void *queue);
void byte_queue_write(void *queue, uint8_t data);

void byte_queue_clear(void *queue);

#endif // !byte_queue_H
