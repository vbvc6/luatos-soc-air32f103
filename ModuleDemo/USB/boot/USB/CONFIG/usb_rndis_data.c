#include "usb_rndis_data.h"
#include "rndis_interface.h"
#include "rndis_protocol.h"
#include <stddef.h>

// uint8_t g_read_buffer[RNDIS_RX_BUFFER_SIZE];

// void usb_rndis_init(void)
// {
// 	_read_buffer_len = 0;
// 	instance = this;
// 	connect();
// 	while (ready() == false) {
// 		Thread::wait(1);
// 	}
// 	TCPIP_Init();
// 	Thread::wait(200);
// }

void usb_rndis_write(uint8_t *buffer, uint32_t size)
{
	uint32_t cur, pos = 0, left = size;
	static uint8_t packet[USB_CDC_DATA_SIZE];
	rndis_data_packet_t *header = (rndis_data_packet_t *)packet;

	for (int i = 0; i < USB_CDC_DATA_SIZE; i++) {
		packet[i] = 0;
	}
	header->MessageType = REMOTE_NDIS_PACKET_MSG;
	header->MessageLength = sizeof(rndis_data_packet_t) + size;
	header->DataOffset = sizeof(rndis_data_packet_t) - (uint32_t)offsetof(rndis_data_packet_t, DataOffset);
	header->DataLength = size;
	cur = size + sizeof(rndis_data_packet_t);
	if (cur > USB_CDC_DATA_SIZE) {
		cur = USB_CDC_DATA_SIZE;
	}
	memcpy(&packet[sizeof(rndis_data_packet_t)], buffer, cur - sizeof(rndis_data_packet_t));
	pos = cur - sizeof(rndis_data_packet_t);
	left = size - pos;
	usb_rndis_write_raw(packet, cur);
	
	while (left > 0) {
		cur = left;
		if (cur > USB_CDC_DATA_SIZE) {
			cur = USB_CDC_DATA_SIZE;
		}
		usb_rndis_write_raw(buffer + pos, cur);
		left = left - cur;
		pos = pos + cur;
	}
	if (cur == USB_CDC_DATA_SIZE) {
		usb_rndis_write_raw(buffer, 0);
	}
}

// void processData(void)
// {
// 	int len = readData(&_read_buffer[_read_buffer_len]);

// 	_read_buffer_len = _read_buffer_len + len;
// 	if ((len != USB_CDC_DATA_SIZE) || (_read_buffer_len == RNDIS_RX_BUFFER_SIZE)){
// 		rndis_data_packet_t *header = (rndis_data_packet_t *)_read_buffer;
// 		uint32_t offset = header->DataOffset + offsetof(rndis_data_packet_t, DataOffset);
// 		// if (_read_buffer_len != (offset + header->DataLength)) {
// 		// 	__BKPT(0);
// 		// }
// 		PacketArrive(&_read_buffer[offset], header->DataLength);
// 		_read_buffer_len = 0;
// 	}
// }

void PacketArrive(uint8_t *buffer, uint32_t size) 
{
	// printf("rndis_PacketArrive line:%d buffer:0x%p size:%d\r\n", __LINE__, buffer, size);	
	// while (size > 2000) {
	// 	__BKPT(0);
	// }
	ethernetif_input(buffer, size);
}

// void rndis_writeData(uint8_t *buffer, uint32_t size)
// {
// 	// printf("rndis_writeData line:%d buffer:0x%p size:%d\r\n", __LINE__, buffer, size);	
// 	USBRndisCtrlData::instance->writePacket(buffer, size);
// 	// printf("%s line:%d buffer:0x%p size:%d ========start===\r\n", __FUNCTION__, __LINE__, buffer, size);	

// }

// USBRndisCtrlData *USBRndisCtrlData::instance;
