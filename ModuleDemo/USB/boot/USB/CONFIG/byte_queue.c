#include <string.h>
#include <stdlib.h>
#include "byte_queue.h"

struct byte_queue {
	uint32_t read;
	uint32_t write;
	uint32_t num;
	uint32_t size;
	uint8_t *items;
};

void *byte_queue_create(uint32_t num)
{
	struct byte_queue *queue = (struct byte_queue *)malloc(sizeof(struct byte_queue));
	
	queue->read = 0;
	queue->write = 0;
	queue->num = num;
	queue->items = (uint8_t *)malloc(num);
	return queue;
}

void byte_queue_distroy(void *queue)
{
	struct byte_queue *byte_queue = queue;

	free(byte_queue->items);
	free(queue);
}

uint32_t byte_queue_count(void *queue)
{
	struct byte_queue *byte_queue = queue;
	uint32_t count;
	
	count = byte_queue->num + byte_queue->write - byte_queue->read;
	if (count >= byte_queue->num) {
		count -= byte_queue->num;
	}
	return count;
}

int byte_queue_full(void *queue)
{
	struct byte_queue *byte_queue = queue;
	
	if (byte_queue_count(queue) >= byte_queue->num - 1) {
		return 1;
	}
	return 0;
}

int byte_queue_empty(void *queue)
{
	return byte_queue_count(queue) == 0;
}

uint8_t byte_queue_read(void *queue)
{
	struct byte_queue *byte_queue = queue;
	uint8_t data = byte_queue->items[byte_queue->read];
	
	uint32_t next = byte_queue->read + 1;
	if (next >= byte_queue->num) {
		next = 0;
	}
	byte_queue->read = next;
	return data;
}

void byte_queue_write(void *queue, uint8_t data)
{
	struct byte_queue *byte_queue = queue;

	byte_queue->items[byte_queue->write] = data;
	uint32_t next = byte_queue->write + 1;
	if (next >= byte_queue->num) {
		next = 0;
	}
	byte_queue->write = next;
}

void byte_queue_clear(void *queue)
{
	struct byte_queue *byte_queue = queue;

	byte_queue->read = 0;
	byte_queue->write = 0;
}
